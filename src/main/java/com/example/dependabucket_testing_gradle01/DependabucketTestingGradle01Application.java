package com.example.dependabucket_testing_gradle01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DependabucketTestingGradle01Application {

	public static void main(String[] args) {
		SpringApplication.run(DependabucketTestingGradle01Application.class, args);
	}

}
